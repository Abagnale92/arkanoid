#include "pch.h"
#include "GameObject.h"
#include "Collider.h"

GameObject::GameObject() :_position(0.0f, 0.0f), _velocity(0.0f, 0.0f), _size(0.0f, 0.0f)
{
	_velocity = DirectX::SimpleMath::Vector2(0.0f, 0.0f);
}

GameObject::GameObject(Vector2 position) : _position(position), _velocity(0.0f, 0.0f), _size(0.0f, 0.0f)
{
	_velocity = DirectX::SimpleMath::Vector2(0.0f, 0.0f);
}

GameObject::GameObject(Vector2 position, Vector2 size) : _position(position), _velocity(0.0f, 0.0f), _size(size)
{
	_velocity = DirectX::SimpleMath::Vector2(0.0f, 0.0f);
}

GameObject::GameObject(Vector2 position, Vector2 size, DirectX::XMVECTORF32 color) : _position(position), _velocity(0.0f, 0.0f), _size(size), _color(color)
{
	_velocity = DirectX::SimpleMath::Vector2(0.0f, 0.0f);
}

GameObject::GameObject(Vector2 position, Vector2 size, float padding, DirectX::XMVECTORF32 color, DirectX::XMVECTORF32 paddingColor) : _position(position), _velocity(0.0f, 0.0f), _size(size), _color(color)
{
	_velocity = DirectX::SimpleMath::Vector2(0.0f, 0.0f);
}

void GameObject::Update(float detaTime, const KeyboardState& keyboard)
{

	if (_velocity.Length() >= MIN_VELOCITY) {
		_position += (_velocity * detaTime);
	}

	for (Collider* collider : _colliders) {
		collider->Update();
	}
}

void GameObject::Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{
	DirectX::VertexPositionColor v1(Vector2(_position.x - _size.x / 2, _position.y - _size.y / 2), _color);
	DirectX::VertexPositionColor v2(Vector2(_position.x + _size.x / 2, _position.y - _size.y / 2), _color);
	DirectX::VertexPositionColor v3(Vector2(_position.x + _size.x / 2, _position.y + _size.y / 2), _color);
	DirectX::VertexPositionColor v4(Vector2(_position.x - _size.x / 2, _position.y + _size.y / 2), _color);
	batch->DrawQuad(v1, v2, v3, v4);
	for (Collider* collider : _colliders) {
		collider->Draw(batch);
	}
}

std::vector<Collider*> GameObject::getColliders()
{
	return _colliders;
}

void GameObject::OnCollisionStart(Collider* obj1, Collider* obj2)
{
}

void GameObject::OnCollisionStay(Collider* obj1, Collider* obj2)
{
}

void GameObject::OnCollisionEnd(Collider* obj1, Collider* obj2)
{
}

typename GameObject::Vector2 GameObject::getPosition()
{
	return _position;
}

typename GameObject::Vector2 GameObject::getSize()
{
	return _size;
}

typename GameObject::Vector2 GameObject::getVelocity()
{
	return _velocity;
}

void GameObject::setVelocity(Vector2 velocity)
{
	_velocity = velocity;
}

void GameObject::setPosition(Vector2 position)
{
	_position = position;
}

GameObject::~GameObject()
{
	for (Collider* collider : _colliders) {
		if (collider != nullptr) {
			delete collider;
			collider = nullptr;
		}
	}
	_colliders.clear();
}
