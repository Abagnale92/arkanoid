
#include "pch.h"
#include "Brick.h"
#include "GameController.h"
#include "Collider.h"

Brick::Brick(Vector2 position) :GameObject(position, Vector2(80.0f, 30.0f), DirectX::Colors::Coral)
{

	_colliders.push_back(new Collider(this, GameController::CollisionTag::brick, Vector2(0.0f, 0.0f), _size));
}

void Brick::Update(float deltaTime, const KeyboardState& keyboard)
{
	GameObject::Update(deltaTime, keyboard);
}

void Brick::Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{
	GameObject::Draw(batch);
}

void Brick::OnCollisionStart(Collider* obj1, Collider* obj2)
{
	for (Collider* collider : _colliders) {
		if (collider != nullptr) {
			delete collider;
			collider = nullptr;
		}
	}
	_colliders.clear();
	_color = DirectX::Colors::Transparent;

	GameController::GetInstance().decrementNumOfBriks();
}
