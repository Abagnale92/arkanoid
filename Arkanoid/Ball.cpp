#include "pch.h"
#include "Ball.h"
#include "Collider.h"
#include "GameController.h"
#include <random>

Ball::Ball(Vector2 position) :GameObject(position, Vector2(15.0f, 15.0f), DirectX::Colors::Green)
{
	_colliders.push_back(new Collider(this, GameController::CollisionTag::ball, Vector2(0.0f, 0.0f), _size));

	std::random_device dev;
	std::mt19937 rng(dev());
	std::uniform_int_distribution<std::mt19937::result_type> dist(0, 90);
	float randomAngle = (float)(dist(rng) - 45) * 3.141592653589793f / 180.0f;

	setVelocity(Vector2::Transform(Vector2(0.0f, -1.0f),
		DirectX::SimpleMath::Matrix::CreateRotationZ(randomAngle)) * m_speed);
}

void Ball::Update(float deltaTime, const KeyboardState& keyboard)
{
	m_isXSwitch = false;
	m_isYSwitch = false;
	GameObject::Update(deltaTime, keyboard);
}

void Ball::Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{
	GameObject::Draw(batch);
}

void Ball::OnCollisionStart(Collider* obj1, Collider* obj2)
{
	GameObject::OnCollisionStart(obj1, obj2);

	if (obj2->getCollisionTag() == GameController::CollisionTag::deathZone) {
		setVelocity(Vector2(0.0f, 0.0f));
		if (GameController::GetInstance().getNumOfLifes() < 0) {
			GameController::GetInstance().gameOver();
		}
		else {
			for (Collider* collider : _colliders) {
				if (collider != nullptr) {
					delete collider;
					collider = nullptr;
				}
			}
			_colliders.clear();
			_color = DirectX::Colors::Green;
			GameController::GetInstance().decrementLives();
			GameController::GetInstance().restartBall();
		}
	}
	else {
		bool revertX = false, revertY = false;
		DirectX::SimpleMath::Rectangle intersection = DirectX::SimpleMath::Rectangle::Intersect(obj1->getWorldRectangle(), obj2->getWorldRectangle());
		if (intersection.width >= intersection.height && !m_isYSwitch) {
			revertY = true;
			m_isYSwitch = true;
		}
		else if (!m_isXSwitch) {
			revertX = true;
			m_isXSwitch = true;
		}
		if (revertX) {
			// cambio direzione della velocita lungo x
			setVelocity(Vector2(-getVelocity().x, getVelocity().y));
		}
		if (revertY) {
			// cambio direzione della velocita lungo y 
			setVelocity(Vector2(getVelocity().x, -getVelocity().y));
		}

		if (obj2->getCollisionTag() == GameController::CollisionTag::paddle) {
			//_ambient = std::make_unique<DirectX::SoundEffect>(GameController::GetInstance().getAudioEngine().get(), L"arkpad.wav");
			//_ambient->Play();
			float devAngle = (_position.x - obj2->getOwner()->getPosition().x) / obj2->getOwner()->getSize().x / 2;
			devAngle *= 75.0f;

			devAngle = devAngle * 3.141592653589793f / 180.0f;
			setVelocity(Vector2::Transform(Vector2(0.0f, -1.0f),
				DirectX::SimpleMath::Matrix::CreateRotationZ(devAngle)) * m_speed);
		}
	}
}

