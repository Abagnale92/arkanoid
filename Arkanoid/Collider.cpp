#include "pch.h"
#include "Collider.h"
#include "GameObject.h"

void Collider::updateCollisions()
{
	if (!m_newCollisions.empty()) {
		m_newCollisions.clear();
	}

	if (!m_oldCollisions.empty()) {
		m_oldCollisions.clear();
	}

	if (!m_finisheddCollisions.empty()) {
		m_finisheddCollisions.clear();
	}
	if (!m_temporaryCollisions.empty()) {
		m_temporaryCollisions.clear();
	}
	for (Collider* collider : GameController::GetInstance().getGameColliders(m_collisionTag)) {
		if (collider->canCollideWith(this)) {
			m_temporaryCollisions.push_back(collider);
			bool found = false;
			for (Collider* oldCollision : m_collisions) {
				if (oldCollision == collider) {
					found = true;
					break;
				}
			}
			if (found) {
				m_oldCollisions.push_back(collider);
			}
			else {
				m_newCollisions.push_back(collider);
			}
		}
	}

	for (Collider* prevCollider : m_collisions) {
		bool found = false;
		for (Collider* oldCollider : m_oldCollisions) {
			if (prevCollider == oldCollider) {
				found = true;
				break;
			}
		}
		if (!found) {
			m_finisheddCollisions.push_back(prevCollider);
		}
	}

	if (!m_collisions.empty()) {
		m_collisions.clear();
	}

	for (Collider* collision : m_temporaryCollisions) {
		m_collisions.push_back(collision);
	}
}

Collider::Collider() :m_owner(nullptr), m_collisionTag(GameController::wall), m_position(0.0f, 0.0f), _size(0.0f, 0.0f)
{
}

Collider::Collider(GameObject* owner, GameController::CollisionTag collisionTag) : m_owner(owner), m_collisionTag(collisionTag), m_position(0.0f, 0.0f), _size(0.0f, 0.0f)
{
}

Collider::Collider(GameObject* owner, GameController::CollisionTag collisionTag, Vector2 position) : m_owner(owner), m_collisionTag(collisionTag), m_position(position), _size(0.0f, 0.0f)
{
}

Collider::Collider(GameObject* owner, GameController::CollisionTag collisionTag, Vector2 position, Vector2 size) : m_owner(owner), m_collisionTag(collisionTag), m_position(position), _size(size)
{
	m_collisions = std::vector<Collider*>();
	m_newCollisions = std::vector<Collider*>();
	m_oldCollisions = std::vector<Collider*>();
	m_finisheddCollisions = std::vector<Collider*>();
	m_temporaryCollisions = std::vector<Collider*>();
}

void Collider::Update()
{
	updateCollisions();

	if (!m_newCollisions.empty()) {
		for (Collider* collider : m_newCollisions) {
			m_owner->OnCollisionStart(this, collider);
		}
	}

	if (!m_oldCollisions.empty()) {
		for (Collider* collider : m_oldCollisions) {
			m_owner->OnCollisionStay(this, collider);
		}
	}

	if (!m_finisheddCollisions.empty()) {
		for (Collider* collider : m_finisheddCollisions) {
			m_owner->OnCollisionEnd(this, collider);
		}
	}
}

void Collider::Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* i_batch) const
{

	float padding = 1.0f;
	DirectX::XMVECTORF32 color = DirectX::Colors::CornflowerBlue;

	DirectX::VertexPositionColor v1(Vector2(m_owner->getPosition().x + m_position.x - _size.x / 2 + padding, m_owner->getPosition().y + m_position.y - _size.y / 2 + padding), color);
	DirectX::VertexPositionColor v2(Vector2(m_owner->getPosition().x + m_position.x + _size.x / 2 - padding, m_owner->getPosition().y + m_position.y - _size.y / 2 + padding), color);
	DirectX::VertexPositionColor v3(Vector2(m_owner->getPosition().x + m_position.x + _size.x / 2 - padding, m_owner->getPosition().y + m_position.y + _size.y / 2 - padding), color);
	DirectX::VertexPositionColor v4(Vector2(m_owner->getPosition().x + m_position.x - _size.x / 2 + padding, m_owner->getPosition().y + m_position.y + _size.y / 2 - padding), color);

	i_batch->DrawQuad(v1, v2, v3, v4);
}

void Collider::setCollisionTag(GameController::CollisionTag collisionTag)
{
	m_collisionTag = collisionTag;
}

GameController::CollisionTag Collider::getCollisionTag()
{
	return m_collisionTag;
}

void Collider::setOwner(GameObject* owner)
{
	m_owner = owner;
}

GameObject* Collider::getOwner()
{
	return m_owner;
}

bool Collider::canCollideWith(Collider* other)
{
	return !DirectX::SimpleMath::Rectangle::Intersect(this->getWorldRectangle(), other->getWorldRectangle()).IsEmpty();
}

DirectX::SimpleMath::Rectangle Collider::getWorldRectangle()
{
	return DirectX::SimpleMath::Rectangle(m_owner->getPosition().x + m_position.x - _size.x / 2, m_owner->getPosition().y + m_position.y - _size.y / 2, _size.x, _size.y);
}
