#pragma once
#include "GameObject.h"
class Paddle :public GameObject {
private:
	float speedPaddle = 500.0f;
public:
	Paddle(Vector2 position);
	virtual void Update(float i_deltaTime, const KeyboardState& i_kb) override;
	virtual void Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const override;
	virtual void OnCollisionStart(Collider* obj1, Collider* obj2) override;
	virtual void OnCollisionStay(Collider* obj1, Collider* obj2) override;
};