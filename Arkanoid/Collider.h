#pragma once
#include "GameController.h"
class GameObject;
class Collider {
protected:
	GameController::CollisionTag m_collisionTag;
	GameObject* m_owner;
	using Vector2 = DirectX::SimpleMath::Vector2;
	Vector2 _size;
	Vector2 m_position;
	std::vector<Collider*>m_collisions;
	std::vector<Collider*>m_newCollisions;
	std::vector<Collider*>m_oldCollisions;
	std::vector<Collider*>m_finisheddCollisions;
	std::vector<Collider*>m_temporaryCollisions;
	void updateCollisions();
public:
	Collider();
	Collider(GameObject* owner, GameController::CollisionTag collisionTag);
	Collider(GameObject* owner, GameController::CollisionTag collisionTag, Vector2 position);
	Collider(GameObject* owner, GameController::CollisionTag collisionTag, Vector2 position, Vector2 size);
	void Update();
	void Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* i_batch) const;
	void setCollisionTag(GameController::CollisionTag collisionTag);
	GameController::CollisionTag getCollisionTag();
	void setOwner(GameObject* owner);
	GameObject* getOwner();
	bool canCollideWith(Collider* other);
	DirectX::SimpleMath::Rectangle getWorldRectangle();
};